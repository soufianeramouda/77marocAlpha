<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChangePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('oldPassword', Type\PasswordType::class, [
                'required' => true,
                'label' => 'form.change_password.label.oldPassword',
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'form.change_password.label.oldPassword'
                ),

            ])
            ->add('plainPassword', Type\RepeatedType::class, [
                'type' => Type\PasswordType::class,
                'first_options'  => [
                    'label' => 'form.change_password.label.plainPassword',
                    'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => 'form.change_password.label.plainPassword'
                    )
                ],
                'second_options' => [
                    'label' => 'form.change_password.label.plainPassword_repeated',
                    'attr' => array(
                        'class' => 'form-control',
                        'placeholder' => 'form.change_password.label.plainPassword_repeated'
                    )
                ],
                'invalid_message' => 'form.change_password.error.incorrect_repeted_password',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
             
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_changepassword';
    }
}

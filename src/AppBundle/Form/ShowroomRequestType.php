<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use AppBundle\Entity\ShowroomRequest;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ShowroomRequestType extends AbstractType
{
  

    function __construct(){
        
      //  $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
         
         $builder->add('name',null,[
               'label' => false,
               'attr' =>[
                           'class'=> 'form-control',
                           'placeholder'=> 'Nom',
                           'required' => false
               ],
          ])->add('phone',null,[
               'label' => false,
               'attr' =>[
                           'class'=> 'form-control',
                           'placeholder'=> 'Telephone',
                           'required' => false
               ],
          ])->add('activity', ChoiceType::class, [
            'label' => false,
            'choices' => [
 
                'Informatique et Multimédia' => ShowroomRequest::INFORMATIQUE,
                'Véhicules' => ShowroomRequest::VEHICULES,
                'Immobilier' => ShowroomRequest::IMMOBILIER,
                'maison et jardin' => ShowroomRequest::MAISON,
                'Habillement et bien être' => ShowroomRequest::HABILLEMENT,
                'Loisirs et Divertissement' => ShowroomRequest::LOISIR,
                'Emploi' => ShowroomRequest::EMPLOIS,
                'Services' => ShowroomRequest::SERVICES,
                'Entreprises' => ShowroomRequest::ENTREPRISE,
                'Autres' => ShowroomRequest::AUTRES,

            ],
        'choices_as_values' => true,
        'placeholder' => 'Sélectionnez Votre Activité',
        'attr' => array(
                  'class' => 'form-control'
            )
        ]);

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\ShowroomRequest'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_showroomrequest';
    }


}

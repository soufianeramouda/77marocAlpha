<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccountTypeType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('validityperiod', null, [
                'attr'  => [
                    'class'    => 'form-control',
                    'required' => true,
                ],
                'label' => 'Durée de validité de l’annonce',
            ])
            ->add('numberofphotos', null, [
                'attr'  => [
                    'class'    => 'form-control',
                    'required' => true,
                ],
                'label' => 'Nombre des photos par annonce',
            ])
            ->add('numberofactiveads', null, [
                'attr'  => [
                    'class'    => 'form-control',
                    'required' => true,
                ],
                'label' => 'Nombre des annonces actives par compte',
            ]);
    }
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\AccountType',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_accounttype';
    }

}

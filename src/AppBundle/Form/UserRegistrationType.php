<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;



class UserRegistrationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('firstName', null, [
                'label' => false,
                'attr'  => [
                    'class'       => 'form-control',
                    'placeholder' => 'Prenom',
                    'required'    => true,
                ],
            ])
            ->add('lastName', null, [
                'label' => false,
                'attr'  => [
                    'class'       => 'form-control',
                    'placeholder' => 'Nom',
                    'required'    => true,
                ],
            ])->add('email', null, [
            'label' => false,
            'attr'  => [
                'class'       => 'form-control',
                'placeholder' => 'Email',
                'required'    => true,
            ],
        ])->add('phone',null,[
                  'label' => false,
                  'attr' => [
                     'class' => 'form-control',
                     'placeholder' => 'Telephone',
                     'required' => true
                    ]
            ])
            ->add('plainPassword', Type\RepeatedType::class, [
                'type'            => Type\PasswordType::class,
                'error_bubbling'  => true,
                'options'         => array('translation_domain' => 'FOSUserBundle'),
                'first_options'   => array('attr' => ['class' => 'form-control',
                    'placeholder'                                 => 'Mot de passe',
                    'required'                                    => $options['required'],
                ],
                ),
                'second_options'  => array('attr' => ['class' => 'form-control',
                    'placeholder'                                 => 'confirmer le mot de passe',
                    'required'                                    => $options['required'],
                ],
                ),
                'invalid_message' => 'Non concordance des mots de passe',
            ])->add('status', ChoiceType::class, array(
                'choices' => array(
                    'Status' => array(
                        'register.status.choice.pro' => true,
                        'register.status.choice.par' => false,
                    ),
                ),
                'attr' => array(
                    'required' => true
                ),
                'expanded' => true,
            ))->add('namevetrine', null, [
                'attr' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Slug Vetrine',
                    'required'    => true,
                ],
            ])->add('logo', FileType::class, array('label' => 'Logo','data_class' => null));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\User',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }

}

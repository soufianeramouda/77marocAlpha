<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GeneralParameterType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('value', null, [
            'attr'  => ['class' => 'form-control',
                'placeholder'       => 'Valeur',
                'required'          => true,
            ],
            'label' => false,
        ]);
    }

    public function configuration(OptionsResolver $resolver)
    {

        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\GeneralParameter',
        ));
    }

    public function getBlockPrefix()
    {
        return 'appbundle_generalparameter';
    }
}

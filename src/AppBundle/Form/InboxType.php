<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use AppBundle\Entity\Inbox;

class InboxType extends AbstractType
{
  

    function __construct(){
        
      //  $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
         
         $builder->add('subject',null,[
               'attr' =>[
                           'class'=> 'form-control',
                           'placeholder'=> 'Sujet',
                           'required' => false
               ],
          ])->add('text',TextareaType::class,[
               'attr' =>[
                           'class'=> 'form-control',
                           'placeholder'=> 'Message',
                           'required' => false
               ],
          ])->add('phone',null,[
               'attr' =>[
                           'class'=> 'form-control',
                           'placeholder'=> 'Phone',
                           'required' => false
               ],
          ])->add('email',null,[
               'attr' =>[
                           'class'=> 'form-control',
                           'placeholder'=> 'Email',
                           'required' => true
               ],
          ]);

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Inbox'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_inbox';
    }


}

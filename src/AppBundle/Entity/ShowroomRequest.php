<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ShowroomRequest
 *
 * @ORM\Table(name="7m_showroom_request")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ShowroomRequestRepository")
 */
class ShowroomRequest
{
    const INFORMATIQUE = "Informatique et Multimédia";
    const VEHICULES = "Véhicules"; 
    const IMMOBILIER = "Immobilier";
    const MAISON = "Pour la maison et jardin";
    const HABILLEMENT = "Habillement et bien être";
    const LOISIR = "Loisirs et Divertissement";
    const EMPLOIS = "Emploi";
    const SERVICES = "Services";
    const ENTREPRISE = "Entreprises";
    const AUTRES = "Autres";

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="activity", type="string", length=255)
     */
    private $activity;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean",nullable=true)
     */
    private $status;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ShowroomRequest
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return ShowroomRequest
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set activity
     *
     * @param string $activity
     *
     * @return ShowroomRequest
     */
    public function setActivity($activity)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return string
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return ShowroomRequest
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }
}

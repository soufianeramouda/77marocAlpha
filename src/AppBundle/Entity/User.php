<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * User
 *
 * @ORM\Table(name="`7m_user`")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser
{ 
    const PERMISSION_BOX_ADS                = "Boites Annonces";
    const PERMISSION_ACCOUNT_TYPE           = "Types Compte";
    const PERMISSION_USERS_NORMAL           = "Comptes Normale";
    const PERMISSION_USERS_VETRINE_ACTIVE   = "Vetrines Actives";
    const PERMISSION_USERS_VETRINE_EXPIRED  = "Vetrines Expirés";
    const PERMISSION_USERS_REQUEST_VETRINE  = "Demandes Vetrine";
    const ROLE_ADMIN_PARAMETER_GENEREUX     = "Parametre generale";

    const PERMISSION_BOITE_ONE              = "Boite annonce 1";
    const PERMISSION_BOITE_TWO              = "Boite annonce 2";
    const PERMISSION_BOITE_THREE            = "Boite annonce 3";
    const PERMISSION_BOITE_FOUR             = "Boite annonce 4";
    const PERMISSION_BOITE_FIVE             = "Boite annonce 5";
    const PERMISSION_BOITE_SIX              = "Boite annonce 6";
    
    const PERMiSSION_BOITE_ACTIVETED        = "Les annonces activer";
    const PERMiSSION_BOITE_SIGNALER         = "Les annonces signaler";
    const PERMiSSION_BOITE_EXPIRER          = "Les annonces expirer";
    const PREMISSION_PUBLICITY              = "Gestion des publicites";
    const ROLE_ADMIN_MESSAGE_TEMPLATE       = "Gestion des messages";
    const PREMISSION_TRANSACTION            = "Gestion des transaction";
    
    /**
     * @ORM\Id
     * @ORM\Column(type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        $this->favads = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
    * @ORM\OneToMany(targetEntity="BoxAds",mappedBy="user")
    */
    private $boxads;
    /**
    * @ORM\ManyToOne(targetEntity="AccountType",inversedBy="user")
    * @ORM\JoinColumn(name="id_acounttype",referencedColumnName="id",nullable=true)
    */
    private $accounttype;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=50,nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=50,nullable=true)
     */
    private $lastname;





    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=50,nullable=true)
     */
    private $phone;

    /**
     * @var bool
     *
     * @ORM\Column(name="isactive", type="boolean",nullable=true)
     */
    private $isactive;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean",nullable=true)
     */
    private $status;

    /**
     * @var float
     *
     * @ORM\Column(name="solde", type="float",nullable=true)
     */
    private $solde;
     /**
     * @var string
     *
     * @ORM\Column(name="`namevetrine`", type="string", length=255,nullable=true)
     */
    private $namevetrine;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true, nullable=true)
     * @Gedmo\Slug(fields={"namevetrine"})
     */
     private $slug;

    /**
    * One User to many Ads
    * @ORM\OneToMany(targetEntity="Ad" , mappedBy="user", cascade={"remove"},fetch="EAGER" )
    */
     private $ad;

    /**
     * @ORM\OneToMany(targetEntity="Inbox", mappedBy="user",cascade={"remove"},fetch="EAGER")  
     */
     private $inbox;

    /**
     * @ORM\OneToMany(targetEntity="Transaction", mappedBy="user",fetch="EAGER")  
     */
    private $Transaction;

    /**
     * Many Users have Many favads.
     * @ORM\ManyToMany(targetEntity="ad")
     * @ORM\JoinTable(name="users_favads",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="favad_id", referencedColumnName="id")}
     *      )
     */
    private $favads;
     
     /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetimetz", options={"default"="CURRENT_TIMESTAMP"},nullable=true)
     */
    private $createdAt;

    /**
     * @var string
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetimetz", options={"default"="CURRENT_TIMESTAMP"},nullable=true)
     */
    private $updatedAt;

    
    /**
     * @SecurityAssert\UserPassword(
     *     message = "form.change_password.error.incorrect_oldpassword",
     *     
     * )
     */
    private $oldPassword;
    /**
    * @ORM\Column(name="dateexpiration",type="datetime",nullable=true)
    */
    private $dateexpiration;


    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=255, nullable=true)
     */

    private $logo;

    /**
     * @Vich\UploadableField(mapping="logo_user", fileNameProperty="logo")
     *
     * @var File
     */
     private $logoFile;

    public static function getConstPermissions()
    {
     return [
            self::PERMISSION_BOX_ADS                =>   "PERMISSION_BOX_ADS",
            self::PERMISSION_BOITE_ONE              =>   "PERMISSION_BOITE_ONE",
            self::PERMISSION_BOITE_TWO              =>   "PERMISSION_BOITE_TWO",
            self::PERMISSION_BOITE_THREE            =>   "PERMISSION_BOITE_THREE",
            self::PERMISSION_BOITE_FOUR             =>   "PERMISSION_BOITE_FOUR",
            self::PERMISSION_BOITE_FIVE             =>   "PERMISSION_BOITE_FIVE",
            self::PERMISSION_BOITE_SIX              =>   "PERMISSION_BOITE_SIX",
            self::PERMiSSION_BOITE_ACTIVETED        =>   "PERMiSSION_BOITE_ACTIVETED",
            self::PERMiSSION_BOITE_SIGNALER         =>   "PERMiSSION_BOITE_SIGNALER",
            self::PERMiSSION_BOITE_EXPIRER          =>   "PERMiSSION_BOITE_EXPIRER",
            self::PERMISSION_ACCOUNT_TYPE           =>   "PERMISSION_ACCOUNT_TYPE",
            self::PERMISSION_USERS_NORMAL           =>   "PERMISSION_USERS_NORMAL",
            self::PERMISSION_USERS_VETRINE_ACTIVE   =>   "PERMISSION_USERS_VETRINE_ACTIVE",
            self::PERMISSION_USERS_VETRINE_EXPIRED  =>   "PERMISSION_USERS_VETRINE_EXPIRED",
            self::PERMISSION_USERS_REQUEST_VETRINE  =>   "PERMISSION_USERS_REQUEST_VETRINE",
            self::ROLE_ADMIN_PARAMETER_GENEREUX     =>   "ROLE_ADMIN_PARAMETER_GENEREUX",
            self::PREMISSION_PUBLICITY              =>   "PREMISSION_PUBLICITY",
            self::ROLE_ADMIN_MESSAGE_TEMPLATE       =>   "ROLE_ADMIN_MESSAGE_TEMPLATE",
            self::PREMISSION_TRANSACTION            =>   "PREMISSION_TRANSACTION",
        ];
    }
  
        /**
     * Set logo
     *
     * @param string $logo
     * @return user
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param array $path
     */
    public function setLogoFile($logo = null)
    {
         
        if ($logo instanceof File) {
            $this->logoFile = $logo;
        }
          $this->updatedAt = new \Datetime();
    }

    /**
     * @return File
     */
    public function getLogoFile()
    {
        return $this->logoFile;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set isactive
     *
     * @param boolean $isactive
     *
     * @return User
     */
    public function setIsactive($isactive)
    {
        $this->isactive = $isactive;

        return $this;
    }

    /**
     * Get isactive
     *
     * @return boolean
     */
    public function getIsactive()
    {
        return $this->isactive;
    }

/****************************/

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set solde
     *
     * @param float $solde
     *
     * @return User
     */
    public function setSolde($solde)
    {
        $this->solde = $solde;

        return $this;
    }

    /**
     * Get solde
     *
     * @return float
     */
    public function getSolde()
    {
        return $this->solde;
    }

    /**
     * Add boxad
     *
     * @param \AppBundle\Entity\BoxAds $boxad
     *
     * @return User
     */
    public function addBoxad(\AppBundle\Entity\BoxAds $boxad)
    {
        $this->boxads[] = $boxad;

        return $this;
    }

    /**
     * Remove boxad
     *
     * @param \AppBundle\Entity\BoxAds $boxad
     */
    public function removeBoxad(\AppBundle\Entity\BoxAds $boxad)
    {
        $this->boxads->removeElement($boxad);
    }

    /**
     * Get boxads
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBoxads()
    {
        return $this->boxads;
    }

    /**
     * Set accounttype
     *
     * @param \AppBundle\Entity\AccountType $accounttype
     *
     * @return User
     */
    public function setAccounttype(\AppBundle\Entity\AccountType $accounttype = null)
    {
        $this->accounttype = $accounttype;

        return $this;
    }

    /**
     * Get accounttype
     *
     * @return \AppBundle\Entity\AccountType
     */
    public function getAccounttype()
    {
        return $this->accounttype;
    }

    /**
     * Set namevetrine
     *
     * @param string $namevetrine
     *
     * @return User
     */
    public function setNamevetrine($namevetrine)
    {
        $this->namevetrine = $namevetrine;

        return $this;
    }

    /**
     * Get namevetrine
     *
     * @return string
     */
    public function getNamevetrine()
    {
        return $this->namevetrine;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return User
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add ad
     *
     * @param \AppBundle\Entity\Ad $ad
     *
     * @return User
     */
    public function addAd(\AppBundle\Entity\Ad $ad)
    {
        $this->ad[] = $ad;

        return $this;
    }

    /**
     * Remove ad
     *
     * @param \AppBundle\Entity\Ad $ad
     */
    public function removeAd(\AppBundle\Entity\Ad $ad)
    {
        $this->ad->removeElement($ad);
    }

    /**
     * Get ad
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAd()
    {
        return $this->ad;
    }

    /*****************/

    /**
     * Add favad
     *
     * @param \AppBundle\Entity\Ad $ad
     *
     * @return User
     */
    public function addFavads(\AppBundle\Entity\Ad $ad)
    {
        $this->favads[] = $ad;

        return $this;
    }

    /**
     * Remove favad
     *
     * @param \AppBundle\Entity\Ad $ad
     */
    public function removeFavads(\AppBundle\Entity\Ad $ad)
    {
        $this->favads->removeElement($ad);
    }

    /**
     * Get favad
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFavads()
    {
        return $this->favads;
    }


    /*****************/

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add inbox
     *
     * @param \AppBundle\Entity\Inbox $inbox
     *
     * @return User
     */
    public function addInbox(\AppBundle\Entity\Inbox $inbox)
    {
        $this->inbox[] = $inbox;

        return $this;
    }

    /**
     * Remove inbox
     *
     * @param \AppBundle\Entity\Inbox $inbox
     */
    public function removeInbox(\AppBundle\Entity\Inbox $inbox)
    {
        $this->inbox->removeElement($inbox);
    }

    /**
     * Get inbox
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInbox()
    {
        return $this->inbox;
    }

     /**
     * Set oldPassword.
     *
     * @param string $registerStep
     *
     * @return User
     */
    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;

        return $this;
    }

    /**
     * Get oldPassword.
     *
     * @return string
     */
    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    /**
     * Add transaction
     *
     * @param \AppBundle\Entity\Transaction $transaction
     *
     * @return User
     */
    public function addTransaction(\AppBundle\Entity\Transaction $transaction)
    {
        $this->Transaction[] = $transaction;

        return $this;
    }

    /**
     * Remove transaction
     *
     * @param \AppBundle\Entity\Transaction $transaction
     */
    public function removeTransaction(\AppBundle\Entity\Transaction $transaction)
    {
        $this->Transaction->removeElement($transaction);
    }

    /**
     * Get transaction
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransaction()
    {
        return $this->Transaction;
    }

    /**
     * Set dateexpiration
     *
     * @param \DateTime $dateexpiration
     *
     * @return User
     */
    public function setDateexpiration($dateexpiration)
    {
        $this->dateexpiration = $dateexpiration;

        return $this;
    }

    /**
     * Get dateexpiration
     *
     * @return \DateTime
     */
    public function getDateexpiration()
    {
        return $this->dateexpiration;
    }

 

    /**
     * Add favad
     *
     * @param \AppBundle\Entity\ad $favad
     *
     * @return User
     */
    public function addFavad(\AppBundle\Entity\ad $favad)
    {
        $this->favads[] = $favad;

        return $this;
    }

    /**
     * Remove favad
     *
     * @param \AppBundle\Entity\ad $favad
     */
    public function removeFavad(\AppBundle\Entity\ad $favad)
    {
        $this->favads->removeElement($favad);
    }
}

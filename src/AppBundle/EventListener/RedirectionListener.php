<?php
namespace AppBundle\EventListener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Session\Session;
use Doctrine\ORM\EntityManager;



class RedirectionListener{
	
	private $container;
	private $router; 
	private $securityContext;
	private $session;
	protected $em;


	public function __construct(ContainerInterface $container,Session $session,EntityManager $em){
		$this->container = $container;
		$this->router    = $container->get('router');
		$this->securityContext = $container->get('security.token_storage'); 
		$this->session = $session;
		$this->em = $em;
		
	}

	public function onkernelRequest(GetResponseEvent $event){

		$routeCurrent = $event->getRequest()->attributes->get('_route');
	 	$CheckRoutes = [
            'dipost_ad',
         ];
		if(in_array($routeCurrent,$CheckRoutes)){
			
				if(!is_object($this->securityContext->getToken()->getUser())) {
					$this->session->getFlashBag()->add('info',"il faut connecter pour pouvoir Déposer une annonce");
					$event->setResponse(new RedirectResponse($this->router->generate('account_login')));
				}
				else{
					$ads = $this->em->getRepository('AppBundle:Ad')->findBy(['status' => true,'user' => $this->securityContext->getToken()->getUser()]);
		        	$numberOfAds = count($ads);
		            $numberOfAdsConfiguration = $this->securityContext->getToken()->getUser()->getAccounttype()->getNumberofactiveads();
			        if($numberOfAds >= $numberOfAdsConfiguration){
						$this->session->getFlashBag()->add('info',"Vous avez depassé le nombre max des annonces à Déposer");
						$event->setResponse(new RedirectResponse($this->router->generate('ads_page')));
					}
				}
		        	
			   }
			if(($routeCurrent == 'buypack_index' || $routeCurrent == 'renew_index') && !is_object($this->securityContext->getToken()->getUser())){
				$this->session->getFlashBag()->add('info',"Vous devez vous connecter pour faire cette opération");
				$event->setResponse(new RedirectResponse($this->router->generate('account_login')));
			}
			if(($routeCurrent == 'user_registration' || $routeCurrent ==  'account_login') && is_object($this->securityContext->getToken()->getUser())){
				 
				$event->setResponse(new RedirectResponse($this->router->generate('ads_page')));
			}
			/*if($routeCurrent == "account_login")
			{
				dump(1);
				die;
			}*/
			
	}

}
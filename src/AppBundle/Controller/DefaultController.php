<?php

namespace AppBundle\Controller;


use AppBundle\Entity\PublicityPosition;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $lastAds      = $em->getRepository('AppBundle:Ad')->findLatestAds(20);
       
        $publicityTop = $em->getRepository('AppBundle:PublicityPosition')->findOneBy([
            'page'     => PublicityPosition::PAGE_PRINCIPALE,
            'position' => PublicityPosition::PAGE_PRINCIPALE_UNDER_RECHERCHE,
        ]);
        $publicityBottom = $em->getRepository('AppBundle:PublicityPosition')->findOneBy([
            'page'     => PublicityPosition::PAGE_PRINCIPALE,
            'position' => PublicityPosition::PAGE_PRINCIPALE_UNDER_ANNONCES,
        ]);

        return $this->render('Default/index.html.twig', [
          
            'lastAds'         => $lastAds,
            'publicityTop'    => $publicityTop,
            'publicityBottom' => $publicityBottom,

        ]);
    }
    /**
    * @Route("/pq-77maroc/qui-nous-sommes/",name="page_qui_nous_somme")
    */
    public function quiNousSommesAction()
    {
       return $this->render('Default/PageStatic/page_qui_nous_sommes.html.twig');
    }
     /**
    * @Route("/pq-77maroc/aide/",name="page_aide")
    */
    public function aideAction()
    {
       return $this->render('Default/PageStatic/page_aide.html.twig');
    }
     /**
    * @Route("/pq-77maroc/achat-securise/",name="page_achat_securise")
    */
    public function achatSecuriseAction()
    {
       return $this->render('Default/PageStatic/page_achat_securise.html.twig');
    }
     /**
    * @Route("/pq-77maroc/reglement/",name="page_reglement")
    */
    public function reglementAction()
    {
       return $this->render('Default/PageStatic/page_reglement.html.twig');
    }
     /**
    * @Route("/pq-77maroc/publicite/",name="page_publicite")
    */
    public function publiciteAction()
    {
       return $this->render('Default/PageStatic/page_publicite.html.twig');
    }


}

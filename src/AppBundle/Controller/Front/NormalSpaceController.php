<?php

namespace AppBundle\Controller\Front;

use AppBundle\Entity\User;
use AppBundle\Entity\Ad;
use AppBundle\Entity\Inbox;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
* @Route("/espace_normal")
*/
class NormalSpaceController extends Controller
{
    /**
     * @Route("/", name="espace_normal_index")
     */
    public function indexAction(Request $request)
    {
    	$usr= $this->getUser();
        // dump($usr);
        // die();

        return $this->render('Default/Spaces/Normal/Solde.html.twig', [
              'user' => $usr,
        ]);
    }

    /**
     * @Route("/noreaded", name="noreaded_index")
     */
    public function noreadedAction(Request $request)
    {
    	$usr= $this->getUser();
        
        //$inboxes = $usr->getInbox();
        $em = $this->getDoctrine()->getManager();
        $inboxes = $em->getRepository('AppBundle:Inbox')->findNoReaded($usr);
      
       
        return $this->render('Default/Spaces/Normal/NoReadedMessage.html.twig', [
              'noreaded' => count($inboxes),
        ]);
    }

    


     /**
     * @Route("/mes_infos", name="mes_info_index")
     * @Security("has_role('ROLE_NORMAL')")
     */
    public function infosAction(Request $request)
    {
    	$usr= $this->getUser();
        $form = $this->createForm('AppBundle\Form\UserRegistrationType',$usr);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->getDoctrine()->getManager()->flush();
            $this->redirectToRoute('mes_info_index');
        }

        return $this->render('Default/Spaces/Normal/Infos.html.twig', [
              'user' => $usr,
              'form' => $form->createView()
        ]);
    }
    
    /**
    * @Route("/liste_annonces/{type}", name="annonces_normale_liste_index",defaults={"type"=1})
    * @Security("has_role('ROLE_NORMAL')")
    */
    public function enabledAdsAction(Request $request,$type){
         

         $em = $this->getDoctrine()->getManager();
         $usr = $this->getUser();

         $numberOfAdsAllowed = $usr->getAccounttype()->getNumberofactiveads();
         if($type == "actives"){
               $lads = $em->getRepository('AppBundle:Ad')->findBy(['user' => $usr,'status' => true],['createdAt' => 'DESC']);
         }else if($type == "inactives"){
               $lads = $em->getRepository('AppBundle:Ad')->findBy(['user' => $usr,'status' => false],['createdAt' => 'DESC']);
         }else{
               $lads = $em->getRepository('AppBundle:Ad')->findBy(['user' => $usr,'status' => null],['createdAt' => 'DESC']);
         }
         $numberofads = count($lads);
         $paginator  = $this->get('knp_paginator');
         $ads = $paginator->paginate(
                    $lads,
                    $request->query->getInt('page',1),
                    intVal(5)
         );
      
         return $this->render('Default/Spaces/Normal/ListAds.html.twig', [
              
              'numberOfAdsAllowed' => $numberOfAdsAllowed,
              'ads' => $ads,
              'type' => $type,
              'numberofads' => $numberofads
         ]);

    }
    
    /**
    * @Route("/disablead/{id}", name="normal_disable_ad")
    */
    public function disableAdAction(Ad $ad)
    {
        $em = $this->getDoctrine()->getManager();
        if($ad){
             $ad->setStatus(false);
             $em->flush();
             return $this->redirectToRoute('annonces_normale_liste_index', ['type' => 'actives']);
        }
    }
      
    /**
    * @Route("/enablead/{id}", name="normal_enable_ad")
    */
    public function enableAdAction(Ad $ad)
    {
        $em = $this->getDoctrine()->getManager();
        if($ad){
             $ad->setStatus(true);
             $em->flush();
             return $this->redirectToRoute('annonces_normale_liste_index', ['type' => 'inactives']);
        }
    }

    /**
     * @Route("/recharger_compte_normal", name="sold_payment_normal_index")
     */
    public function SoldPaymentAction(Request $request)
    {
     
      
        return $this->render('Default/Spaces/Normal/SoldePayment.html.twig');
    }

    /**
     * @Route("/inbox", name="inbox_normal_index")
     */
    public function inboxAction(Request $request)
    {
     
        $usr = $this->getUser();
        $inboxlist = $usr->getInbox();
        $paginator  = $this->get('knp_paginator');
        $inbox = $paginator->paginate(
            $inboxlist,
            $request->query->getInt('page',1),
            intVal(5)
        );
        return $this->render('Default/Spaces/Normal/inbox.html.twig', [
            
            'inbox' => $inbox
        ]);
    }

     /**
     * @Route("/read/{in}", options ={"expose" = true},
     * condition="request.isXmlHttpRequest()" ,name="normal_read_message")
     * @Method({"POST"})
     */
    public function editSoldeUserNormalOrvetrine(Inbox $in)
    {
         
        $currentuser = $this->getUser();
        if ($currentuser == $in->getUser()) {
            $in->setReadedAt(new \DateTime(date('Y-m-d H:i:s')));
            $in->setReaded(true);
            
            $this->getDoctrine()->getManager()->flush();
            return new JsonResponse(['success' => true]);
        } else {
            return new JsonResponse(['success' => false]);
        }

    }

       /**
     * @Route("/delete_multiple", options ={"expose" = true},
     * condition="request.isXmlHttpRequest()" ,name="delete_multiple_message")
     * @Method({"POST"})
     */
    public function deletemultiplemessageAction(Request $request)
    {
        //dump($request->get("ids"));
        //die;
        $em = $this->getDoctrine()->getManager();
        $ids = $request->get("ids");
        $deleted = array();
        
        foreach ($ids as $id) {
            $inbox = $em->getRepository('AppBundle:Inbox')->findOneBy(array('id' => $id));
            $em->remove($inbox);
            array_push($deleted, $id);
        }
        $em->flush();
        return new JsonResponse($deleted);

    }

    /**
     * @Route("/change_password", name="normal_edit_password_index")
     */
    public function editPasswordAction(Request $request)
    {
       
        $form = $this->createForm('AppBundle\Form\ChangePasswordType',$this->getUser());
        $form->handleRequest($request);
        //dump($this->getUser());
        //die();
        if ($form->isValid()) {
         
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($this->getUser());

            $this->addFlash(
                'success',
                'alert.saved.success'
            );

            $this->redirectToRoute('normal_edit_password_index');
        }

        return $this->render('Default/Spaces/Normal/ChangePassword.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/transactions", name="normal_Transaction__index")
     */
    public function TransactionAction(Request $request)
    {
     
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        //$transactions = $em->getRepository('AppBundle:Transaction')->findAll();
        $transactionslist = $user->getTransaction();

        $paginator  = $this->get('knp_paginator');
        $transactions = $paginator->paginate(
            $transactionslist,
            $request->query->getInt('page',1),
            intVal(5)
        );
        return $this->render('Default/Spaces/Normal/Transactions.html.twig', [
            
            'transactions' => $transactions
        ]);
    }


    /**
    * @Route("/favads", name="favads_index")
    * @Security("has_role('ROLE_NORMAL')")
    */
    public function favAdsAction(Request $request){
         

        
        $usr = $this->getUser();
        $favads = $usr->getFavads();
         
     
        $paginator  = $this->get('knp_paginator');
        $ads = $paginator->paginate(
                   $favads,
                   $request->query->getInt('page',1),
                   intVal(5)
        );
     
        return $this->render('Default/Spaces/Normal/Favads.html.twig', [
             

             'ads' => $ads,
       
             
        ]);

   }

    
}

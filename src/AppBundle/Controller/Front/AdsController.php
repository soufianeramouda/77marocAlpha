<?php

namespace AppBundle\Controller\Front;

use AppBundle\Entity\Ad;
use AppBundle\Entity\Category;
use AppBundle\Entity\ImagesAd;
use AppBundle\Entity\Inbox;
use AppBundle\Entity\PublicityPosition;
use AppBundle\Services\AutoDetectedBoiteAnnonce;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/annonces")
 */
class AdsController extends Controller
{
    /**
     * @Route("/", name="ads_page")
     * @Route("/{categoryfather}", name="ads_category_index", defaults={"categoryfather":1})
     * @Route("/filter/ads/", name="filter_global_ad")
     */
    public function indexAction(Request $request)
    {
        $em      = $this->getDoctrine()->getManager();
        $adsList = $em->getRepository('AppBundle:Ad')->findAdsCritiriaActivited();

        if (array_key_exists('Keyword', $request->query->all())) {
            $adsList = $em->getRepository('AppBundle:Ad')->filteredAds($request->query->all());
        } elseif ($request->attributes->get('categoryfather')) {
            $adsList = $em->getRepository('AppBundle:Ad')->findAdsCritiriaCategoryFather($request->attributes->get('categoryfather'));
        }

        $numberAllAds = count($adsList);

        $adsPar = array_filter($adsList, function ($ad) {
            return $ad->getType() == 'par';
        });
        $numberParAds = count($adsPar);

        $adsPro = array_filter($adsList, function ($ad) {
            return $ad->getType() == 'pro';
        });
        $numberProAds = count($adsPro);

        $type = $request->query->get('type');
        if ($type) {
            $adsList = array_filter($adsList, function ($ad) use ($type) {
                return $ad->getType() == $type;
            });
        }
        $now = new \DateTime(date('Y-m-d H:i:s'));

        $adsFeatureList   = null;
        $adsFeatureFilter = array_filter($adsList, function ($ad) use ($now) {
            return $ad->getInleaduntil() != null && $ad->getInleaduntil() > $now;
        });
        if (!empty($adsFeatureFilter)) {
            if (count($adsFeatureFilter) > 3) {
                $adsFeatureList = $this->array_random_assoc($adsFeatureFilter);
            } else {
                $adsFeatureList = $adsFeatureFilter;
            }
        }

        $ConfigAdsOfPage = $em->getRepository('AppBundle:GeneralParameter')->findOneBy(['name' => 'NUMBER_OF_ADS_PER_PAGE']);
        $numberAdsOfPage = $ConfigAdsOfPage->getValue() ?: 10;

        $paginator = $this->get('knp_paginator');
        $ads       = $paginator->paginate(
            $adsList,
            $request->query->getInt('page', 1),
            intVal($numberAdsOfPage)
        );
        $user = $this->getUser();
        $favads = "";
        if($user){
            $favads = $user->getFavads();
        }
        
        return $this->render('Default/Ads/Ads.html.twig', array(

            'ads'            => $ads,
            'numberAllAds'   => $numberAllAds,
            'numberParAds'   => $numberParAds,
            'numberProAds'   => $numberProAds,
            'adsFeatureList' => $adsFeatureList,
            'favads'         => $favads,
        ));
    }

    /**
     *@Route("/details/{slug}",name="detail_ad",defaults={"slug"=1},options={ "expose" = true })
     *@ParamConverter("ad", options={"mapping": {"slug": "slug"}})
     */
    public function detailAdIndex(Request $request, Ad $ad)
    {

            $em = $this->getDoctrine()->getManager();

           if($ad->getStatus() == false || $ad->getStatus() == null)
             {
                if(!$this->getUser()){
                     return $this->redirect($this->generateUrl('ads_page'));
                }
                if(!$this->getUser()->getUsername() == $ad->getUser()->getUsername())
                {
                    return $this->redirect($this->generateUrl('ads_page'));
                
                } 
               
             }else{
                  $ad->setNumbervisit($ad->getNumbervisit() + 1);
                  $this->getDoctrine()->getManager()->flush();
             }
                   
      
            
        $inbox = new Inbox();

        $form = $this->createForm('AppBundle\Form\InboxType', $inbox);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $inbox->setType(Inbox::ANNONYME);
            $inbox->setSubject($ad->getTitle());
            $inbox->setUser($ad->getUser());
            $em->persist($inbox);
            $em->flush();
            return $this->redirectToRoute('detail_ad', ['slug' => $ad->getSlug()]);
        }

        $publicityTop = $em->getRepository('AppBundle:PublicityPosition')->findOneBy([
            'page'     => PublicityPosition::PAGE_ANNONCE,
            'position' => PublicityPosition::PAGE_ANNONCE_UNDER_TITLE,
        ]);
        $publicityContact = $em->getRepository('AppBundle:PublicityPosition')->findOneBy([
            'page'     => PublicityPosition::PAGE_ANNONCE,
            'position' => PublicityPosition::PAGE_ANNONCE_UNDER_CONTACT,
        ]);
        $publicityBottom = $em->getRepository('AppBundle:PublicityPosition')->findOneBy([
            'page'     => PublicityPosition::PAGE_ANNONCE,
            'position' => PublicityPosition::PAGE_ANNONCE_BELOW_ANNONCE,
        ]);

        return $this->render('Default/Ads/adDetail.html.twig', ['ad' => $ad,
            'publicityTop'                                               => $publicityTop,
            'publicityContact'                                           => $publicityContact,
            'publicityBottom'                                            => $publicityBottom,
            "form"                                                       => $form->createView(),
        ]);
    }
    /**
     *@Route("/details/signaler/{id}",name="detail_ad_signaler")
     *@ParamConverter("ad", options={"mapping": {"id": "id"}})
     */
    public function SignalerAdindex(Request $request, Ad $ad)
    {
        $ad->setNumbersignaler($ad->getNumbersignaler() + 1);
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('info', 'Nous avons reçu votre déclaration signalement de cette annonce ;et nous le traitrons le plustot possible');
        return $this->redirect($this->generateUrl('detail_ad', array('slug' => $ad->getSlug())));
    }

    /**
     *@Route("/deposer-annonce/", name="dipost_ad")
     */
    public function dipostAdAction(Request $request)
    {
        return $this->render('Default/Ads/depositAd.html.twig');
    }

    /**
     * @Route("/deposer-annonce/trait",name="dipost_ad_trait")
     * @Security("has_role('ROLE_NORMAL') or has_role('ROLE_VETRINE') or has_role('ROLE_SUPER_ADMIN')")
     */
    public function dipostAdtraitAction(Request $request)
    {

        $em                       = $this->getDoctrine()->getManager();
        $ad                       = new Ad();
        $AutoDetectedBoiteAnnonce = $this->get(AutoDetectedBoiteAnnonce::class);
        $ad->setUser($this->getUser());
        $ad->setBoxad($AutoDetectedBoiteAnnonce->detectedBoite());
        $ad->setPrice($request->request->get('price'));
        $ad->setCategoryfather($request->request->get('fathercategory'));

        if ($this->getUser()->hasRole('ROLE_VETRINE')) {
            $ad->setType('pro');
        }

        if($this->getUser()->hasRole('ROLE_NORMAL') && $this->getUser()->getStatus())
         {
            $ad->setType('pro');
        }else{
            $ad->setType('par');
        }

   

        if (null == $request->request->get('numeroshow')) {
            $ad->setNumero($this->getUser()->getPhone());
        }
        $ad->setListoptions($request->request->get('critiriaOption'));

        foreach ($request->files as $key => $file) {
            if (!empty($file)) {
                $image = new ImagesAd();
                $image->setPathFile($file);
                $image->setAds($ad);
                $image->setAlt($key);
                $em->persist($image);
                $ad->addImage($image);
            }
        }
        $ad->setCategory($request->request->get('categories'));
        $ad->setTitle($request->request->get('title'));
        $ad->setDescription($request->request->get('description'));
        $ad->SetTypeannonce('Offre');
        $ad->setUrlwebsite($request->request->get('lienWebSite'));
        $ad->setUrlyoutube($request->request->get('lienYoutube'));
        $ad->setLatlng($request->request->get('latLng'));

        $ad->setVillename($request->request->get('ville'));
        $ad->setSecteurname($request->request->get('region'));
        $ad->setQuartier($request->request->get('quartier'));

        $validator = $this->get('validator');
        $errors    = $validator->validate($ad);
        if (count($errors) > 0) {
            throw new NotFoundHttpException('Sorry not existing!');
        } else {
            $em->persist($ad);
            $em->flush();
            $ad->setSlug($ad->getSlug());
            $em->flush();

          
            return $this->redirectToRoute('dipost_ad_trait_sucess');

        }

    }
    /**
     * @Route("/deposer-annonce/sucess",name="dipost_ad_trait_sucess")
     */
    public function successAdDepoAction(Request $request)
    {
        return $this->render('Default/PagesSucces/deposer_ad_secess.html.twig');
    }

    public function array_random_assoc($arr, $num = 3)
    {
        $keys = array_keys($arr);
        shuffle($keys);

        $r = array();
        for ($i = 0; $i < $num; $i++) {
            $r[$keys[$i]] = $arr[$keys[$i]];
        }
        return $r;
    }


     /**
     * @Route("/favad/{id}",options= {"expose" = true},
     * condition="request.isXmlHttpRequest()",name="favad_index")
     */
    public function FavAdAction(Request $request, Ad $ad)
    {
        if ($ad) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();
            $favads = $user->getFavads();
            if ($favads->contains($ad)){
                $user->removeFavads($ad);
             }else{
                $user->addFavads($ad);
             }
            $em->flush();
          
            return new JsonResponse(['success' => true]);
        } else {
            return new JsonResponse(['success' => false]);
        }

    }

}

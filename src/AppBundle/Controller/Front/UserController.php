<?php

namespace AppBundle\Controller\Front;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/")
 */

class UserController extends Controller
{
    /**
     * @Route("/account/registration",name="user_registration")
     * @Method({"GET", "POST"})
     */
    public function RegisterAction(Request $request)
    {

        $userManager = $this->container->get('fos_user.user_manager');
        $user        = $userManager->createUser();

        $formCreateUser = $this->createForm('AppBundle\Form\UserRegistrationType', $user);
        $formCreateUser->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $accounttype1 = $em->getRepository('AppBundle:AccountType')->findOneBy(['id' => 1]);
       
        if ($formCreateUser->isSubmitted()) {

            
            $user->setUsername($user->getEmail());
            $user->setRoles(["ROLE_NORMAL"]);
            $user->setAccounttype($accounttype1);
            $user->setEnabled(true);
            $userManager->updateUser($user);
            
            return $this->redirectToRoute('account_login');
        }

        return $this->render('Default/User/Register.html.twig', array(
            'form' => $formCreateUser->createView(),
        ));

    }
}

<?php

namespace AppBundle\Controller\Front;

use AppBundle\Entity\User;
use AppBundle\Entity\Ad;
use AppBundle\Entity\PaidOptions;
use AppBundle\Entity\Transaction;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Translation\TranslatorInterface;
use \Datetime;

/**
* @Route("/pricing")
*/
class PricingController extends Controller
{
     
    /**
     * @Route("/always_in_lead/{ad}", name="always_index",defaults={"ad"=1})
     */
    public function alwaysAction(Request $request,Ad $ad)
    {
        $em = $this->getDoctrine()->getManager(); 
        $renewpack = $em->getRepository('AppBundle:PaidOptions')->findBy(['type' => PaidOptions::TYPE_ALWAYS_IN_LEAD]);
        return $this->render('Default/PricingModel/AlwaysPricingModel.html.twig',[
         'packs' => $renewpack,
         'ad' => $ad

        ]);
    }

     

    /**
     * @Route("/buypack/{po}/{ad}", name="buypack_index",defaults={"po"=1,"ad"=1})
     */
    public function buypackAction(Request $request,PaidOptions $po,Ad $ad)
    {
        // dump($po);
        // die();
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        if($ad->getUser() != $user){
            $this->addFlash("warning", $this->get('translator')->trans('pack.message_alert.ad_for_another_user'));
            return $this->redirectToRoute("detail_ad", ["slug" => $ad->getSlug()]);
       }else{
           return $this->render('Default/PricingModel/BuyPack.html.twig',[
            'pack' => $po,
            'ad' => $ad
           ]);
       }
        
       
    }

    /**
     * @Route("/always_choose_payment/{po}/{ad}", name="always_choose_payment_index",defaults={"po"=1,"ad"=1})
     */
    public function alwayschoosepaymentAction(Request $request,PaidOptions $po,Ad $ad)
    {
        $now = new \DateTime(date('Y-m-d H:i:s'));
        $user = $this->getUser();
        $always_in_lead_price = "";
        $currentSolde = "";
        $em = $this->getDoctrine()->getManager();
        
        if($ad->getUser() != $user){
            $this->addFlash("info", $this->get('translator')->trans('pack.message_alert.ad_for_another_user'));
            return $this->redirectToRoute("detail_ad", ["slug" => $ad->getSlug()]);
       }else{
           $always_in_lead_price = $po->getPrice();
           $currentSolde = $user->getSolde();
       }
       
       if($ad->getInleaduntil() > $now){
        $this->addFlash("warning","Votre annonce deja tous jour en tete jusqu : ".$ad->getInleaduntil()->format('Y-m-d H:i:s'));
        return $this->redirectToRoute("detail_ad", ["slug" => $ad->getSlug()]);
       }else{

        if($request->request->get('payment-type')){
             
              if($request->request->get('payment-type') == 'card'){
                return $this->render('Default/PricingModel/AlwaysByCard.html.twig',[
                    'ad' => $ad,
                    'pack' => $po
                ]);
              }

              if($request->request->get('payment-type') == 'sold'){
   
                if($currentSolde < $always_in_lead_price){
            
                    $this->addFlash('warning', $this->get('translator')->trans('pack.message_alert.ad_please_fill_your_account'));
                    return $this->redirectToRoute('always_choose_payment_index', ['po'=>$po->getId(),'ad'=>$ad->getId()]);
                }

                return $this->render('Default/PricingModel/AlwaysBySold.html.twig',[
                    'ad' => $ad,
                    'pack' => $po
                ]);
              }
          

           }
   

           return $this->render('Default/PricingModel/ChooseAlwaysOnLeadPayment.html.twig',[
            'pack' => $po,
            'ad' => $ad
           ]);
       }
        
       
    }

    /**
     * @Route("/renew/{ad}", name="renew_index",defaults={"ad"=1})
     */
    public function renewadAction(Request $request,Ad $ad)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager(); 
        
         if($ad->getUser() == $user){
               return $this->render('Default/PricingModel/RenewAd.html.twig',[
                'ad' => $ad
       
               ]);
          }else{
              $this->addFlash("info", $this->get('translator')->trans('pack.message_alert.ad_for_another_user'));
              return $this->redirectToRoute("detail_ad", ["slug" => $ad->getSlug()]);
          }
    }

     /**
     * @Route("/renew_choose_payment_methode/{ad}", name="renew_choose_payment_methode_index",defaults={"ad"=1})
     */
    public function choosepaymentAction(Request $request,Ad $ad)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager(); 
        $currentSolde = "";
        $renewprice = $this->getParameter("renew_price");
    
         if($ad->getUser() == $user){
               $currentSolde = $user->getSolde();
              
              if($request->request->get('payment-type')){
                 
                  if($request->request->get('payment-type') == 'card'){
                    return $this->render('Default/PricingModel/RenewByCard.html.twig',[
                        'ad' => $ad
                    ]);
                  }
                  if($request->request->get('payment-type') == 'sold'){
                    
                    if($currentSolde < $renewprice){
                        $this->addFlash('warning', $this->get('translator')->trans('pack.message_alert.ad_please_fill_your_account'));
                        return $this->redirectToRoute('renew_choose_payment_methode_index', ['ad'=>$ad->getId()]);
                    }

                    return $this->render('Default/PricingModel/RenewBySold.html.twig',[
                        'ad' => $ad
                    ]);
                  }
              
               }

               return $this->render('Default/PricingModel/ChooseRenewPayment.html.twig',[
                'ad' => $ad
       
               ]);
          }else{
              $this->addFlash("info", $this->get('translator')->trans('pack.message_alert.ad_for_another_user'));
              return $this->redirectToRoute("detail_ad", ["slug" => $ad->getSlug()]);
          }
        
        
        
    }


    /**
     * @Route("/paymentpackbysolde/{ad}/{po}", name="paymentpackbysolde_index",defaults={"ad"=1,"po"=1})
     */
    public function paymentpackbysoldAction(Ad $ad,PaidOptions $po)
    {
          $em = $this->getDoctrine()->getManager();
          $user = $this->getUser();
          $transaction = new Transaction();
          $currentSolde = $user->getSolde();
          $always_in_lead_price = $po->getPrice();
          $now = new \DateTime(date('Y-m-d H:i:s'));
          $nowstring = $now->format('Y-m-d H:i:s');
          $numberofdays = strval($po->getValue());
          if($ad->getUser() != $user){
            $this->addFlash("warning", $this->get('translator')->trans('pack.message_alert.ad_for_another_user'));
            return $this->redirectToRoute("detail_ad", ["slug" => $ad->getSlug()]);
           }else{
            //   dump(new \DateTime(date('Y-m-d H:i:s',strtotime($nowstring. "+".$numberofdays." day"))));
            //   die;
            $dataend = new \DateTime(date('Y-m-d H:i:s',strtotime($nowstring. "+".$numberofdays." day")));
                 /** nouvelle transaction **/
            $transaction->setTotal($always_in_lead_price);
            $transaction->setUser($user);
            $transaction->setType(Transaction::ACHAT_TOUS_JOUR_EN_TETE);
            $transaction->setPaymentby(Transaction::PAYMENT_PAR_SOLDE);
            $transaction->setStatus(Transaction::STATUS_DONE);
            $transaction->setIdad($ad->getSlug());
            /** Ajouter la date de fin de l'option tous jour en tete + renouvellement de date **/
            $ad->setInleaduntil($dataend);
            $ad->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
            
            /** debité le solde **/
            $user->setSolde($currentSolde - $always_in_lead_price);
            $em->persist($transaction);
            $em->flush();
           // $this->addFlash('success',$this->get('translator')->trans('pack.message_alert.ad_please_fill_your_account '.$dataend->format('Y-m-d H:i:s')));
           return $this->redirectToRoute('thankyou_index',["type" => "always","ad"=>$ad->getId()]);
          } 
    }

    /**
     * @Route("/paymentrenewbysolde/{ad}", name="paymentrenewbysolde_index",defaults={"ad"=1})
     */
    public function paymentrenewbysoldeAction(Ad $ad)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $transaction = new Transaction();
        $currentSolde = $user->getSolde();
        $renewprice = $this->getParameter("renew_price");
        if($ad->getUser() != $user){
               $this->addFlash("warning", $this->get('translator')->trans('pack.message_alert.ad_for_another_user'));
               return $this->redirectToRoute("detail_ad", ["slug" => $ad->getSlug()]);
          }else{
   
            /** nouvelle transaction **/
            $transaction->setTotal($renewprice);
            $transaction->setUser($user);
            $transaction->setType(Transaction::ACHAT_DE_RENOUVELLEMENT);
            $transaction->setPaymentby(Transaction::PAYMENT_PAR_SOLDE);            
            $transaction->setStatus(Transaction::STATUS_DONE);
            $transaction->setIdad($ad->getSlug());
            /** changer la date de creation d'annonce (renouveller) **/
            $ad->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
            $ad->setRenewed(true);
            
            /** debité le solde **/
            $user->setSolde($currentSolde - $renewprice);
            $em->persist($transaction);
            $em->flush();
            
                   
            
            
            return $this->redirectToRoute('thankyou_index',["type" => "renew", "ad"=>$ad->getId()]);
            
          }
    }

    

    /**
     * @Route("/after_renew/{ad}", name="after_renew_index",defaults={"ad"=1})
     * 
    */
    public function afterrenewpaymentAction(Ad $ad){
              
        return $this->render("Default/PricingModel/RenewAfterPayment.html.twig", [
            'ad' => $ad
        ]);
    }

    /**
     * @Route("/thankyou/{type}/{ad}", name="thankyou_index",defaults={"type"=1,"ad"=2})
     * 
    */
    public function thankyouAction($type,Ad $ad){
              
        return $this->render("Default/PricingModel/ThankyouPage/Renew.html.twig", [
            'type' => $type,
            'ad' => $ad
        ]);
    }


    /*****************************************************************************************
    *                                                                                        *
    ********************************* /\ Attractive Ad /\ ************************************
    *                                                                                        *
    *****************************************************************************************/

    /**
     * @Route("/attractivead_payment_methode/{ad}", name="attractivead_choose_payment_methode_index",defaults={"ad"=1})
     */
    public function chooseattractiveadpaymentAction(Request $request,Ad $ad)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager(); 
        
        $AttractiveAdprice = $this->getParameter("Attractive_ad_price");
    
         if($ad->getUser() == $user){
            $currentSolde = $user->getSolde();
              if($request->request->get('payment-type')){
                 
                  if($request->request->get('payment-type') == 'card'){
                    return $this->render('Default/PricingModel/Attractive_ad/AttractiveAdByCard.html.twig',[
                        'ad' => $ad
                    ]);
                  }
                  if($request->request->get('payment-type') == 'sold'){
                    
                    if($currentSolde < $AttractiveAdprice){
                        $this->addFlash('warning', $this->get('translator')->trans('pack.message_alert.ad_please_fill_your_account'));
                        return $this->redirectToRoute('attractivead_choose_payment_methode_index', ['ad'=>$ad->getId()]);
                    }

                    return $this->render('Default/PricingModel/Attractive_ad/AttractiveAdBySold.html.twig',[
                        'ad' => $ad
                    ]);
                  }
              
               }

               return $this->render('Default/PricingModel/Attractive_ad/ChooseAttractiveAdPayment.html.twig',[
                'ad' => $ad
       
               ]);
          }else{
              $this->addFlash("info", $this->get('translator')->trans('pack.message_alert.ad_for_another_user'));
              return $this->redirectToRoute("detail_ad", ["slug" => $ad->getSlug()]);
          }   
    }


    /**
     * @Route("/paymentattractivead/{ad}", name="paymentattractiveAdbysolde_index",defaults={"ad"=1})
     */
    public function paymentattractiveadbysoldeAction(Ad $ad)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $transaction = new Transaction();
        $currentSolde = $user->getSolde();
        $AttractiveAdprice = $this->getParameter("Attractive_ad_price");
        if($ad->getUser() != $user){
               $this->addFlash("warning", $this->get('translator')->trans('pack.message_alert.ad_for_another_user'));
               return $this->redirectToRoute("detail_ad", ["slug" => $ad->getSlug()]);
          }else{
   
            /** nouvelle transaction **/
            $transaction->setTotal($AttractiveAdprice);
            $transaction->setUser($user);
            $transaction->setType(Transaction::ACHAT_DE_RENOUVELLEMENT);
            $transaction->setPaymentby(Transaction::PAYMENT_PAR_SOLDE);            
            $transaction->setStatus(Transaction::STATUS_DONE);
            $transaction->setIdad($ad->getSlug());
            /** changer la date de creation d'annonce (renouveller) **/
            $ad->setAttractive(true);
            
            /** debité le solde **/
            $user->setSolde($currentSolde - $AttractiveAdprice);
            $em->persist($transaction);
            $em->flush();
            //$this->addFlash('success', $this->get('translator')->trans('pack.message_alert.AttractiveAd_ad'));
            return $this->redirectToRoute('thankyou_index',["type" => "attr","ad"=>$ad->getId()]);
            
            
          }
    }

    /**
     * @Route("/before_attractivead/{ad}", name="after_attractivead_index",defaults={"ad"=1})
     * 
    */
    public function afterattractiveadpaymentAction(Ad $ad){
              
        return $this->render("Default/PricingModel/Attractive_ad/AttractiveAdAfterPayment.html.twig", [
            'ad' => $ad
        ]);
    }


    /*****************************************************************************************
    *                                                                                        *
    ********************************* /\ Recharge Account /\ ************************************
    *                                                                                        *
    *****************************************************************************************/

    /**
     * @Route("/recharge_account_payment_methode", name="recharge_account_choose_payment_methode_index",defaults={"ad"=1})
     */
    public function rechargeaccountadpaymentAction(Request $request)
    {
        $user = $this->getUser();
         
         if($user){
            if($request->request->get('payment-type')){
                return $this->render('Default/PricingModel/Recharge_account/RechargeAccountByCard.html.twig');
            }
        
            return $this->render('Default/PricingModel/Recharge_account/ChooseRechargeAccountPayment.html.twig');
          }else{
              $this->addFlash("info", $this->get('translator')->trans('pack.message_alert.ad_for_another_user'));
              return $this->redirectToRoute("detail_ad", ["slug" => $ad->getSlug()]);
          }   
    }
     

    
}

<?php

namespace AppBundle\Controller\Front;

use AppBundle\Entity\User;
use AppBundle\Entity\Ad;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
* @Route("/espace_vitrine")
*/
class ShowroomSpaceController extends Controller
{
    /**
     * @Route("/", name="espace_vitrine_index")
     */
    public function indexAction(Request $request)
    {
    	$usr= $this->getUser();
        // dump($usr);
        // die();

        return $this->render('Default/Spaces/Showroom/Solde.html.twig', [
              'user' => $usr,
        ]);
    }

     /**
     * @Route("/ex", name="expiration_vitrine_index")
     */
    public function expirationAction(Request $request)
    {
        $usr= $this->getUser();
        $expirationdata = $usr->getUpdatedAt()->modify('+30 day');
        
        return $this->render('Default/Spaces/Showroom/Expiration.html.twig', [
              'expirationdata' => $expirationdata,
        ]);
    }


     /**
     * @Route("/vitrine_infos", name="vitrine_info_index")
     */
    public function infosAction(Request $request)
    {
         
    	$usr= $this->getUser();
        $form = $this->createForm('AppBundle\Form\UserRegistrationType',$usr);
        $form->handleRequest($request);
       
        if($form->isSubmitted()){
            $logofile = $usr->getLogo();
            // dump($logofile);
            // die;
            if(!empty($logofile)){
               $usr->setLogo($logofile);
               $usr->setLogoFile($logofile);
            }
            $this->getDoctrine()->getManager()->persist($usr);
            $this->getDoctrine()->getManager()->flush();
            $this->redirectToRoute('vitrine_info_index');
        }

        return $this->render('Default/Spaces/Showroom/Infos.html.twig', [
              'user' => $usr,
              'form' => $form->createView()
        ]);
    }
    
    /**
    * @Route("/liste_annonces/{type}", name="annonces_vitrine_liste_index", defaults={"type"=1})
    */
    public function enabledAdsAction(Request $request,$type){
         

         $em = $this->getDoctrine()->getManager();
         $usr = $this->getUser();
      
         $numberOfAdsAllowed = $usr->getAccounttype()->getNumberofactiveads();
         if($type == "actives"){
               $lads = $em->getRepository('AppBundle:Ad')->findBy(['user' => $usr,'status' => true],['createdAt' => 'DESC']);
         }else if($type == "inactives"){
               $lads = $em->getRepository('AppBundle:Ad')->findBy(['user' => $usr,'status' => false],['createdAt' => 'DESC']);
         }else{
               $lads = $em->getRepository('AppBundle:Ad')->findBy(['user' => $usr,'status' => null],['createdAt' => 'DESC']);
         }
         $numberofads = count($lads);
         $paginator  = $this->get('knp_paginator');
         $ads = $paginator->paginate(
                    $lads,
                    $request->query->getInt('page',1),
                    intVal(5)
         );
         
         return $this->render('Default/Spaces/Showroom/ListAds.html.twig', [
              
              'numberOfAdsAllowed' => $numberOfAdsAllowed,
              'ads' => $ads,
              'type' => $type,
              'numberofads' => $numberofads
         ]);

    }
    
    /**
    * @Route("/disablead/{id}", name="show_disable_ad")
    */
    public function disableAdAction(Ad $ad)
    {
        $em = $this->getDoctrine()->getManager();
        if($ad){
             $ad->setStatus(false);
             $em->flush();
             return $this->redirectToRoute('annonces_vitrine_liste_index', ['type' => 'actives']);
        }
    }

    /**
    * @Route("/enablead/{id}", name="show_enable_ad")
    */
    public function enableAdAction(Ad $ad)
    {
        $em = $this->getDoctrine()->getManager();
        if($ad){
             $ad->setStatus(true);
             $em->flush();
             return $this->redirectToRoute('annonces_vitrine_liste_index', ['type' => 'inactives']);
        }
    }

    /**
     * @Route("/inbox", name="inbox_index")
     */
    public function inboxAction(Request $request)
    {
      
        $usr = $this->getUser();
        $inboxlist = $usr->getInbox();
        $paginator  = $this->get('knp_paginator');
        $inbox = $paginator->paginate(
            $inboxlist,
            $request->query->getInt('page',1),
            intVal(5)
        );
        return $this->render('Default/Spaces/Showroom/inbox.html.twig', [
            
            'inbox' => $inbox
        ]);
    }

    /**
     * @Route("/recharger_compte_showroom", name="sold_payment_showroom_index")
     */
    public function SoldPaymentAction(Request $request)
    {
     
      
        return $this->render('Default/Spaces/Showroom/SoldePayment.html.twig');
    }


    /**
     * @Route("/change_password", name="edit_password_index")
     */
    public function editPasswordAction(Request $request)
    {
       
        $form = $this->createForm('AppBundle\Form\ChangePasswordType',$this->getUser());
        $form->handleRequest($request);
        //dump($this->getUser());
        //die();
        if ($form->isValid()) {
         
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($this->getUser());

            $this->addFlash(
                'success',
                'alert.saved.success'
            );

            $this->redirectToRoute('edit_password_index');
        }

        return $this->render('Default/Spaces/Showroom/ChangePassword.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/transactions", name="Transaction__index")
     */
    public function TransactionAction(Request $request)
    {
     
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        //$transactions = $em->getRepository('AppBundle:Transaction')->findAll();
        $transactions = $user->getTransaction();
        return $this->render('Default/Spaces/Showroom/Transactions.html.twig', [
            
            'transactions' => $transactions
        ]);
    }

    /**
    * @Route("/favads", name="showroom_favads_index")
    * @Security("has_role('ROLE_VETRINE')")
    */
    public function favAdsAction(Request $request){
         

        
        $usr = $this->getUser();
        $favads = $usr->getFavads();
         
     
        $paginator  = $this->get('knp_paginator');
        $ads = $paginator->paginate(
                   $favads,
                   $request->query->getInt('page',1),
                   intVal(5)
        );
     
        return $this->render('Default/Spaces/Showroom/Favads.html.twig', [
             'ads' => $ads,     
        ]);

   }

    
}

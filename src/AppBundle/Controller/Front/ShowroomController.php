<?php

namespace AppBundle\Controller\Front;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
* @Route("/")
*/
class ShowroomController extends Controller
{
    /**
     * @Route("/boutiques", name="showroompage")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $showroomsList = $em->getRepository('AppBundle:User')->findBy(array("accounttype" => 2));
        
        $paginator = $this->get('knp_paginator');
        $showrooms       = $paginator->paginate(
            $showroomsList,
            $request->query->getInt('page', 1),
            intVal(10)
        );

        return $this->render('Default/Showroom/Showroom.html.twig',array(
              'showrooms' => $showrooms,
        ));
        
    }

    /**
     *@Route("/{slug}",name="showroomads_index",defaults={"slug"=1})
     *@ParamConverter("ad", options={"mapping": {"slug": "slug"}})
     */
    public function showroomadsAction(Request $request,User $usr)
    {
        $em = $this->getDoctrine()->getManager();
        
        $adsList = $em->getRepository('AppBundle:Ad')->findBy(array("user" => $usr,'status' => true));
        //dump($ads);
        //die();

        $paginator = $this->get('knp_paginator');
        $ads       = $paginator->paginate(
            $adsList,
            $request->query->getInt('page', 1),
            10
        );
        

        return $this->render('Default/Showroom/ShowroomAds.html.twig',array(
              'ads' => $ads,
              'user' => $usr
        ));
        
    }

  
}

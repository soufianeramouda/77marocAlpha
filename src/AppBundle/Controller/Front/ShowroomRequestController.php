<?php

namespace AppBundle\Controller\Front;

use AppBundle\Entity\ShowroomRequest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/")
 */

class ShowroomRequestController extends Controller
{
    /**
     * @Route("/vitrine/demande_vitrine",name="showroomrequest_index")
     * @Method({"GET", "POST"})
     */
    public function AddRequestAction(Request $request)
    {
 
        $Showroomrequest = new ShowroomRequest();
        $form = $this->createForm("AppBundle\Form\ShowroomRequestType",$Showroomrequest);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();

        if($form->isSubmitted() && $form->isValid()){

        $em->persist($Showroomrequest);
        $em->flush();

        return $this->redirectToRoute("ads_page");
        }

        return $this->render('Default/ShowroomRequest/ShowroomRequest.html.twig',[

            'form' => $form->createView()

        ]);

             

    }
}

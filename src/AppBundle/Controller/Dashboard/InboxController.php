<?php

namespace AppBundle\Controller\Dashboard;

use AppBundle\Entity\Inbox;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * City controller.
 *
 * @Route("dashboard/inbox")
 */
class InboxController extends Controller
{


   /**
     * @Route("/", name="inbox_dashboard_index")
     */
    public function inboxAction(Request $request)
    {
     
        $em = $this->getDoctrine()->getManager();
        $inbox = $em->getRepository('AppBundle:Inbox')->findBy([ 'type' => ['toadmin','sysmessage'] ]);

        return $this->render('Dashboard/Inbox/inbox.html.twig', [
            
            'inbox' => $inbox
        ]);
    }

}

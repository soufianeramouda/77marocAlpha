<?php
namespace AppBundle\Controller\Dashboard;

use AppBundle\Entity\AccountType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/dashboard/accounttype")
 */
class AccountTypeController extends Controller
{
    /**
     * @Route("/",name="account_type_list")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function indexAction(Request $request)
    {
        $em             = $this->getDoctrine()->getManager();
        $accountNormal  = $em->getRepository('AppBundle:AccountType')->findOneBy(['accountwording' => 'NORMAL']);
        $accountVetrine = $em->getRepository('AppBundle:AccountType')->findOneBy(['accountwording' => 'VETRINE']);

        $formNormal  = $this->createCreateForm($accountNormal);
        $formVetrine = $this->createCreateForm($accountVetrine);

        return $this->render("Dashboard/AccountType/AccountType.html.twig", [
            'formAccountNormal'  =>
            $formNormal->createView(),
            'accountNormal'      => $accountNormal,
            'formAccountVetrine' =>
            $formVetrine->createView(),
            'accountVetrine'     => $accountVetrine,
        ]);
    }

    /**
     * @Route("/edit/{id}",name="edit_config_account")
     * @Method({"POST"})
     */
    public function editAccountAction(Request $request, AccountType $accountType)
    {
        $form = $this->createCreateForm($accountType);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Operation a été passé avec succés');
        }
        return $this->redirect($this->generateUrl('account_type_list'));
    }

    protected function createCreateForm(AccountType $acountType)
    {
        $form = $this->createForm('AppBundle\Form\AccountTypeType', $acountType);
        return $form;
    }

}

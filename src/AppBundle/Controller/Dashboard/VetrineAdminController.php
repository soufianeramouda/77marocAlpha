<?php

namespace AppBundle\Controller\Dashboard;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/dashboard/vetrine")
 */

class VetrineAdminController extends Controller
{

    /**
     * @Route("/list",name="users_vetrine_list")
     * @Security("has_role('ROLE_SUPER_ADMIN') or  has_role('PERMISSION_USERS_VETRINE_ACTIVE')")
     */
    public function listUsersVetrineAction(Request $request)
    {

        $em           = $this->getDoctrine()->getManager();
        $usersVetrine = $em->getRepository('AppBundle:User')->findUsersByRole("ROLE_VETRINE");
        return $this->render('Dashboard/Vetrine/user.html.twig', [
            'users' => $usersVetrine,
        ]);
    }

    /**
     * @Route("/add",name="users_vetrine_add")
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('PERMISSION_USERS_VETRINE_ACTIVE')")
     * @Method({"GET", "POST"})
     */
    public function addVetrineAction(Request $request)
    {

        $userManager    = $this->container->get('fos_user.user_manager');
        $user           = $userManager->createUser();
        $formCreateUser = $this->createForm('AppBundle\Form\UserType', $user, ['normalOrVetrine' => true, 'isVetrine' => true,'isAddVetrine' => true]);
        $formCreateUser->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        
        if ($formCreateUser->isSubmitted()) {
            $user->setPassword($user->getPlainPassword());
            $user->setStatus(false);
            $now = new \DateTime(date('Y-m-d H:i:s'));
            $Validityperiod = $formCreateUser->getData()->getDateexpiration();
            date_add($now, date_interval_create_from_date_string($Validityperiod.' days'));
            $dateValidityVetrine = new \DateTime(date_format($now, 'Y-m-d H:i:s'));
            $user->setDateexpiration($dateValidityVetrine);
            $accounttype2 = $em->getRepository('AppBundle:AccountType')->findOneBy(['id' => 2]);
            $user->setUsername($user->getEmail());
            $user->setRoles(["ROLE_VETRINE"]);
            $user->setAccounttype($accounttype2);

            $userManager->updateUser($user);
            $this->addFlash('success', 'Noveau utilisateur Vetrine a été ajouté avec succés');
            return $this->redirectToRoute('users_vetrine_list');
        }

        return $this->render('Dashboard/Vetrine/AddUser.html.twig', array(
            'user'           => $user,
            'formCreateUser' => $formCreateUser->createView(),
        ));

    }

    /**
     * @Route("/{id}/edit", name="users_vetrine_edit")
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('PERMISSION_USERS_VETRINE_ACTIVE')")
     * @Method({"GET", "POST"})
     */
    public function editVetrineAction(Request $request, User $user)
    {
        $userManager  = $this->container->get('fos_user.user_manager');
        $formEditUser = $this->createForm('AppBundle\Form\UserType', $user, ['normalOrVetrine' => true,
            'required'                                                                             => false, 'isVetrine' => true]);
        $formEditUser->handleRequest($request);

        if ($formEditUser->isSubmitted()) {
            $user->setPassword($user->getPlainPassword());
            $user->setStatus(false);
           
            $userManager->updateUser($user);
            $this->addFlash('success', 'La modification a été passé avec succés');
            return $this->redirectToRoute('users_vetrine_list');
        }

        return $this->render('Dashboard/Vetrine/EditUser.html.twig', array(
            'user'         => $user,
            'formEditUser' => $formEditUser->createView(),
        ));

    }
    /**
     * @Route("/expirer/", name="users_vetrine_expirer")
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('PERMISSION_USERS_VETRINE_EXPIRED')")
     */
    public function VetrineExpirerAction(Request $request)
    {
          $em        = $this->getDoctrine()->getManager();
          $users = $em
                ->createQuery("SELECT usr FROM AppBundle:User usr WHERE usr.dateexpiration < CURRENT_DATE() and 
                    usr.roles LIKE '%ROLE_VETRINE%'")
                ->getResult();
          foreach ($users as $ad) {
              $ad->setEnabled(false);
          }

         $em->flush();

         return $this->render('Dashboard/Vetrine/vetrine_expirer.html.twig', array('users' => $users));

    }


    /**
     * @Route("/{id}/delete/vetrine", name="users_vetrine_delete")
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('PERMISSION_USERS_VETRINE_EXPIRED')")
     */
    public function deleleVetrineAction(Request $request, User $user)
    {
        $userManager  = $this->container->get('fos_user.user_manager');
        $userManager->deleteUser($user);
        $this->addFlash('sucess','votre operation a été passé avec sucess');
        return $this->redirect($this->generateUrl('users_vetrine_expirer'));
    }
     /**
     * @Route("/{id}/activer/vetrine", name="users_vetrine_activer")
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('PERMISSION_USERS_VETRINE_EXPIRED')")
     */
    public function activerVetrineAction(Request $request, User $user)
    {
         $now = new \DateTime(date('Y-m-d H:i:s'));
         $Validityperiod = $request->request->get('duree_validiter');
         date_add($now, date_interval_create_from_date_string($Validityperiod.' days'));
         $dateValidityVetrine = new \DateTime(date_format($now, 'Y-m-d H:i:s'));
         $user->setDateexpiration($dateValidityVetrine);
         $user->setEnabled(true);
         $this->getDoctrine()->getManager()->flush();
         $this->addFlash('sucess','votre operation a été passé avec sucess');
         return $this->redirect($this->generateUrl('users_vetrine_expirer'));
    }


}

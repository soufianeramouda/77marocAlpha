<?php

namespace AppBundle\Controller\Dashboard;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Ad;

/**
 * @Route("/dashboard")
 */

class DashboardController extends Controller
{
    /**
     * @Route("/", name="dashboard_index")
     */
    public function indexAction(Request $request)
    {
		
    	$em = $this->getDoctrine()->getManager();
    	$ads = $em->getRepository(Ad::class)->findAdsCritiriaActivited();
    	
    	
    	$immobilier_location = array_filter($ads,function($ad){
    		return $ad->getCategoryfather() == "immobilier_location";
    	});
    	$immobilier_vente = array_filter($ads,function($ad){
    		return $ad->getCategoryfather() == "immobilier_vente";
    	});
    	$vehicule = array_filter($ads,function($ad){
    		return $ad->getCategoryfather() == "vehicule";
    	});
    	$emploi_et_service = array_filter($ads,function($ad){
    		return $ad->getCategoryfather() == "emploi_et_service";
    	});
    	$multimedia = array_filter($ads,function($ad){
    		return $ad->getCategoryfather() == "multimedia";
    	});
    	$maison_et_bureau = array_filter($ads,function($ad){
    		return $ad->getCategoryfather() == "maison_et_bureau";
    	});
    	$loisirs = array_filter($ads,function($ad){
    		return $ad->getCategoryfather() == "loisirs";
    	});
    	$mode_et_beaute = array_filter($ads,function($ad){
    		return $ad->getCategoryfather() == "mode_et_beaute";
    	});
    	$business = array_filter($ads,function($ad){
    		return $ad->getCategoryfather() == "business";
    	});
    	$Autres = array_filter($ads,function($ad){
    		return $ad->getCategoryfather() == "Autres";
    	});
    	

        return $this->render('Dashboard/Dashboard.html.twig',[ 'immobilier_location' =>$immobilier_location,
        													   'immobilier_vente' =>$immobilier_vente,
        													   'vehicule' =>$vehicule,
        													   'emploi_et_service' =>$emploi_et_service,
        													   'maison_et_bureau' =>$maison_et_bureau,
        													   'multimedia' =>$multimedia,
        													   'loisirs' =>$loisirs, 
        													   'mode_et_beaute' =>$mode_et_beaute,
        													   'business' =>$business,
        													   'Autres' =>$Autres,
    												
    													]);
    }
}

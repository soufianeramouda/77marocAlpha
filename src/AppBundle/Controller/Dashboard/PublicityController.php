<?php

namespace AppBundle\Controller\Dashboard;

use AppBundle\Entity\PublicityPosition;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/dashboard/publicity")
 */

class PublicityController extends Controller
{
    /**
     * @Route("/", name="index_publicity")
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('ROLE_ADMIN_PUBLICITY')")
     */
    public function indexAction(Request $request)
    {
        return $this->render('Dashboard/Publicity/publicity.html.twig');
    }

    /**
     *@Route("/edit/pub/{id}",name="publicity_valider")
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('ROLE_ADMIN_PUBLICITY')")
     */
    public function ValiderPublicityAction(Request $request, PublicityPosition $publicity)
    {
        $form = $this->createForm('AppBundle\Form\PublicityType', $publicity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form['type']->getData() == PublicityPosition::TYPE_PUB_SCRIPT) {
                $publicity->setImage(null);
                $publicity->setUrl(null);
                $this->get('vich_uploader.upload_handler')->remove($publicity, 'imageFile');

            }
            if ($form['type']->getData() == PublicityPosition::TYPE_PUB_IMAGE_URL) {
                $publicity->setScript(null);
            }

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Votre operation a été passé avec succés ');

            return $this->redirectToRoute(
                $form['currentURL']->getData() ?: 'index_publicity'
            );

        }

    }

    /**
     *@Route("/page-principale",name="publicity_page_principale")
     *@Security("has_role('ROLE_SUPER_ADMIN') or has_role('ROLE_ADMIN_PUBLICITY')")
     */
    public function pagePrincipaleAction(Request $request)
    {
        $em            = $this->getDoctrine()->getManager();
        $publicityPPUR = $em->getRepository('AppBundle:PublicityPosition')
            ->findOneBy([
                'page'     => PublicityPosition::PAGE_PRINCIPALE,
                'position' => PublicityPosition::PAGE_PRINCIPALE_UNDER_RECHERCHE,
            ]);

        $publicityPPUA = $em->getRepository('AppBundle:PublicityPosition')
            ->findOneBy([
                'page'     => PublicityPosition::PAGE_PRINCIPALE,
                'position' => PublicityPosition::PAGE_PRINCIPALE_UNDER_ANNONCES,
            ]);
        $formPPUR = $this->createForm('AppBundle\Form\PublicityType', $publicityPPUR,
            [
                'currentURL' => 'publicity_page_principale']
        );
        $formPPUA = $this->createForm('AppBundle\Form\PublicityType', $publicityPPUA, ['currentURL' => 'publicity_page_principale']);

        return $this->render('Dashboard/Publicity/pagePrincipale.html.twig', [
            'formPPUR' => $formPPUR->createView(),
            'pubPPUR'  => $publicityPPUR,
            'formPPUA' => $formPPUA->createView(),
            'pubPPUA'  => $publicityPPUA,
        ]);

    }

    /**
     *@Route("/page-all-annonces",name="publicity_page_all_annonces")
     *@Security("has_role('ROLE_SUPER_ADMIN') or has_role('ROLE_ADMIN_PUBLICITY')")
     */
    public function pageAllAnnoncesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $publicityPAUR = $em->getRepository('AppBundle:PublicityPosition')
            ->findOneBy([
                'page'     => PublicityPosition::PAGE_ALL_ANNONCES,
                'position' => PublicityPosition::PAGE_ALL_ANNONCES_UNDER_RECHERCHE,
            ]);

        $publicityPABR = $em->getRepository('AppBundle:PublicityPosition')
            ->findOneBy([
                'page'     => PublicityPosition::PAGE_ALL_ANNONCES,
                'position' => PublicityPosition::PAGE_ALL_ANNONCES_BELOW_RECHERCHE,
            ]);
        $publicityPABTA = $em->getRepository('AppBundle:PublicityPosition')
            ->findOneBy([
                'page'     => PublicityPosition::PAGE_ALL_ANNONCES,
                'position' => PublicityPosition::PAGE_ALL_ANNONCES_BETWEN_ANNONCES,
            ]);
        $publicityPAEA = $em->getRepository('AppBundle:PublicityPosition')
            ->findOneBy([
                'page'     => PublicityPosition::PAGE_ALL_ANNONCES,
                'position' => PublicityPosition::PAGE_ALL_ANNONCES_ENTER_ANNONCES,
            ]);

        $formPublicityPAUR = $this->createForm('AppBundle\Form\PublicityType', $publicityPAUR,
            [
                'currentURL' => 'publicity_page_all_annonces']
        );
        $formPublicityPABR  = $this->createForm('AppBundle\Form\PublicityType', $publicityPABR, ['currentURL' => 'publicity_page_all_annonces']);
        $formPublicityPABTA = $this->createForm('AppBundle\Form\PublicityType', $publicityPABTA, ['currentURL' => 'publicity_page_all_annonces']);

        $formPublicityPAEA = $this->createForm('AppBundle\Form\PublicityType', $publicityPAEA, ['currentURL' => 'publicity_page_all_annonces']);

        return $this->render('Dashboard/Publicity/pageAllAnnonces.html.twig', [
            'formPublicityPAUR'  => $formPublicityPAUR->createView(),
            'publicityPAUR'      => $publicityPAUR,
            'formPublicityPABR'  => $formPublicityPABR->createView(),
            'publicityPABR'      => $publicityPABR,
            'formPublicityPABTA' => $formPublicityPABTA->createView(),
            'publicityPABTA'     => $publicityPABTA,
            'formPublicityPAEA'  => $formPublicityPAEA->createView(),
            'publicityPAEA'      => $publicityPAEA,
        ]);

    }

    /**
     *@Route("/page-annonce",name="publicity_page_annonce")
     *@Security("has_role('ROLE_SUPER_ADMIN') or has_role('ROLE_ADMIN_PUBLICITY')")
     */
    public function pageAnnonceAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $publicityPAUT = $em->getRepository('AppBundle:PublicityPosition')
            ->findOneBy([
                'page'     => PublicityPosition::PAGE_ANNONCE,
                'position' => PublicityPosition::PAGE_ANNONCE_UNDER_TITLE,
            ]);

        $publicityPABA = $em->getRepository('AppBundle:PublicityPosition')
            ->findOneBy([
                'page'     => PublicityPosition::PAGE_ANNONCE,
                'position' => PublicityPosition::PAGE_ANNONCE_BELOW_ANNONCE,
            ]);
        $publicityPAUC = $em->getRepository('AppBundle:PublicityPosition')
            ->findOneBy([
                'page'     => PublicityPosition::PAGE_ANNONCE,
                'position' => PublicityPosition::PAGE_ANNONCE_UNDER_CONTACT,
            ]);

        $formPublicityPAUT = $this->createForm('AppBundle\Form\PublicityType', $publicityPAUT,
            [
                'currentURL' => 'publicity_page_annonce']
        );
        $formPublicityPABA = $this->createForm('AppBundle\Form\PublicityType', $publicityPABA, [
            'currentURL' => 'publicity_page_annonce']
        );

        $formPublicityPAUC = $this->createForm('AppBundle\Form\PublicityType', $publicityPAUC, [
            'currentURL' => 'publicity_page_annonce']
        );

        return $this->render('Dashboard/Publicity/pageAnnonce.html.twig', [
            'formPublicityPAUT' => $formPublicityPAUT->createView(),
            'publicityPAUT'     => $publicityPAUT,
            'formPublicityPABA' => $formPublicityPABA->createView(),
            'publicityPABA'     => $publicityPABA,
            'formPublicityPAUC' => $formPublicityPAUC->createView(),
            'publicityPAUC'     => $publicityPAUC,

        ]);

    }

}

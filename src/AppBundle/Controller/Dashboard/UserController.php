<?php

namespace AppBundle\Controller\Dashboard;

use AppBundle\Entity\User;
use AppBundle\Entity\Transaction;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\UserRegistrationType;

/**
 * @Route("/dashboard/admins")
 */

class UserController extends Controller
{
    /**
     * @Route("/list", name="users_admins_list")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function listUsersAdminAction(Request $request)
    {
        $em    = $this->getDoctrine()->getManager();
        $users = $em->getRepository('AppBundle:User')->findUsersByRole("ROLE_ADMIN");
        return $this->render('Dashboard/Admins/user.html.twig', ['users' => $users]);
    }

    /**
     * @Route("/add",name="users_admins_add")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @Method({"GET", "POST"})
     */
    public function addAdminAction(Request $request)
    {

        $userManager    = $this->container->get('fos_user.user_manager');
        $user           = $userManager->createUser();
        $formCreateUser = $this->createForm('AppBundle\Form\UserType', $user);
        $formCreateUser->handleRequest($request);

        if ($formCreateUser->isSubmitted() ) {
            $user->setUsername($user->getEmail());
            $user->setPassword($user->getPlainPassword());
            $user->setStatus(false);
            $user->setRoles(["ROLE_ADMIN"]);
            $userManager->updateUser($user);
            $this->addFlash('success', 'Noveau Admin a été ajouté avec succés');
            return $this->redirectToRoute('users_admins_list');
        }

        return $this->render('Dashboard/Admins/AddUser.html.twig', array(
            'user'           => $user,
            'formCreateUser' => $formCreateUser->createView(),
        ));

    }
    /**
     * @Route("/{id}/edit", name="users_admins_edit")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     * @Method({"GET", "POST"})
     */
    public function editAdminAction(Request $request, User $user)
    {
        $userManager  = $this->container->get('fos_user.user_manager');
        $formEditUser = $this->createForm('AppBundle\Form\UserType', $user, ['required' => false]);
        $formEditUser->handleRequest($request);

        if ($formEditUser->isSubmitted()) {
              $user->setPassword($user->getPlainPassword());
            $user->setStatus(false);
            $userManager->updateUser($user);
            $this->addFlash('success', 'La modification a été passé avec succés');
            return $this->redirectToRoute('users_admins_list');
        }

        return $this->render('Dashboard/Admins/EditUser.html.twig', array(
            'user'         => $user,
            'formEditUser' => $formEditUser->createView(),
        ));

    }

    /**
     * @Route("/{id}", options={ "expose" = true },
     * condition="request.isXmlHttpRequest()",name="user_delete")
     * @ParamConverter("User", options={"mapping": {"id": "id"}})
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('PERMISSION_USERS_NORMAL')
    or has_role('PERMISSION_USERS_VETRINE_ACTIVE')")
     */
    public function deleteAction(Request $request, User $user)
    {
        if ($user) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
            return new JsonResponse(['success' => true]);
        } else {
            return new JsonResponse(['success' => false]);
        }
    }
    /**
     * @Route("/{id}/permissions",name="users_admins_perimission")
     * @ParamConverter("User", options={"mapping": {"id": "id"}})
     * @Security("has_role('ROLE_SUPER_ADMIN')")

     */
    public function permissionsAction(Request $request, User $user)
    {

        $userManager     = $this->container->get('fos_user.user_manager');
        $formPermissions = $this->createForm('AppBundle\Form\PermissionsType', $user);
        $formPermissions->handleRequest($request);

        if ($formPermissions->isSubmitted() && $formPermissions->isValid()) {
            $user->addRole('ROLE_ADMIN');

            $userManager->updateUser($user);
            $this->addFlash('success', 'Permissions affecter avec succés');
            return $this->redirect($this->generateUrl('users_admins_perimission', array('id' => $user->getId())));

        }
        return $this->render('Dashboard/Admins/PermissionsUser.html.twig', [
            'formPermissions' => $formPermissions->createView(),
            'user'            => $user,
        ]);

    }

    /**
     * @Route("/edit/{id}/solde", options ={"expose" = true},
     * condition="request.isXmlHttpRequest()" ,name="users_edit_solde")
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('PERMISSION_USERS_NORMAL')
    or has_role('PERMISSION_USERS_VETRINE_ACTIVE') ")
     * @Method({"POST"})
     */
    public function editSoldeUserNormalOrvetrine(Request $request, User $user)
    {
        $solde = $request->request->get('solde');
        $transaction = new Transaction();
        $currentuser = $this->getUser();
        if ($user && $currentuser) {
            $transaction->setTotal($solde);
            $transaction->setReloadedaccount($user->getId());
            $transaction->setUser($currentuser);
            $transaction->setType(Transaction::RECHARGE_DE_SOLDE);
            $transaction->setStatus(Transaction::STATUS_DONE);
            $user->setSolde($solde);
            $this->getDoctrine()->getManager()->persist($transaction);
            $this->getDoctrine()->getManager()->flush();
            return new JsonResponse(['success' => true]);
        } else {
            return new JsonResponse(['success' => false]);
        }

    }

     /**
     * @Route("/account/info", name="mes_info_admin")
     */
    public function infosAddminAction(Request $request)
    {
        $userManager  = $this->container->get('fos_user.user_manager');
        $usr= $this->getUser();
        $form = $this->createForm('AppBundle\Form\UserType',$usr);
        $form->handleRequest($request);
        if($form->isSubmitted()){
             $usr->setPassword($usr->getPlainPassword());
             $usr->setStatus(false);
             $usr->setEnabled(true);
             $userManager->updateUser($usr);
            $this->redirectToRoute('mes_info_admin');
            $this->addFlash('success', 'La modification a été passé avec succés');

        }

        return $this->render('Dashboard/adminAccount.html.twig', [
              'user' => $usr,
              'form' => $form->createView()
        ]);
    }
    

}

<?php

namespace AppBundle\Controller\Dashboard;

use AppBundle\Entity\Inbox;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 *
 * @Route("dashboard/Transaction")
 */
class TransactionController extends Controller
{


   /**
     * @Route("/", name="Transaction_dashboard_index")
     * @Security("has_role('ROLE_SUPER_ADMIN') or has_role('PREMISSION_TRANSACTION')")
     */
    public function TransactionAction(Request $request)
    {
     
        $em = $this->getDoctrine()->getManager();
        $transactions = $em->getRepository('AppBundle:Transaction')->findAll();

        return $this->render('Dashboard/Transaction/Transaction.html.twig', [
            
            'transactions' => $transactions
        ]);
    }

}

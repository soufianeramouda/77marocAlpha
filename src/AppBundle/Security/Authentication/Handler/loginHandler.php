<?php 
namespace AppBundle\Security\Authentication\Handler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
 
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Routing\RouterInterface;
use Doctrine\ORM\EntityManager;


class loginHandler implements AuthenticationSuccessHandlerInterface {

    private $router;
    private $container;
    private $session;
    private static $key;
    private $tokenStorage;

    public function __construct(RouterInterface $router, EntityManager $em, $container) {

        self::$key = '_security.main.target_path';
        $this->router = $router;
        $this->em = $em;
        $this->session = $container->get('session');
        if ($container->has('security.token_storage')) {
            $this->tokenStorage = $container->get('security.token_storage');
        } else {
            $this->tokenStorage = $container->get('security.context');
        }
    }

    public function onAuthenticationSuccess( Request $request, TokenInterface $token ) {

    //    $defaultTargetPath = sprintf('_security.%s.target_path', $this->tokenStorage->getToken()->getProviderKey());
        $refererUrl = $request->get('_target_path');
        
        if (!empty($refererUrl)) {
           
            $route = $refererUrl;
            
        } else{
        
            $url = $this->router->generate('ads_page');
            return new RedirectResponse($url);
           
        }

        return new RedirectResponse($route);

    }
}
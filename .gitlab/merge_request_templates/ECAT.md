Lien JIRA : **{Lien de Requirement}**

### Checklist Merge Request

- [ ] J'ai relu les specs de requirement et mon code correspond à ce qui est demandé.
- [ ] J'ai commité tous les fichiers nécessaires (attention aux nouveaux fichiers).
- [ ] J'ai testé et exporté la configuration (*drush cex*) et commité seulement les fichiers de mon dev (revert les autres).
- [ ] J'ai bien assurer que les configs sont bien dans le bon split (e-cat ou bien webfaactory).
- [ ] J'ai bien exécuté la commande (*blt valide*) et mon est respecte les bonnes pratiques PHP/DRUPAL.
- [ ] J'ai testé mon dev avec les caches activés et avec un utilisateur anonyme et un administrateur.
- [ ] J'ai relu mon code sur la Merge Request.
- [ ] J'ai mis à jour les tâches JIRA.
- [ ] J'ai saisi des com dans ticket/bug Jira pour le front(optinnelll).
- [ ] J'ai défini un message de commit détaillé.
